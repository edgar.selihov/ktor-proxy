package com.bigbank.techOffice

import com.bigbank.techOffice.controller.routeRegistration.registerRoutes
import com.bigbank.techOffice.util.JsonUtil
import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.papsign.ktor.openapigen.OpenAPIGen
import com.papsign.ktor.openapigen.schema.namer.DefaultSchemaNamer
import com.papsign.ktor.openapigen.schema.namer.SchemaNamer
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.application.call
import io.ktor.features.AutoHeadResponse
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.features.CallLogging
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.*
import io.ktor.response.respondText
import kotlin.reflect.KType

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    install(AutoHeadResponse)
    install(CallLogging)

    install(StatusPages) {
        exception<Throwable> { e ->
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode.InternalServerError
            )
        }
    }

    install(ContentNegotiation) {
        jackson {
            registerModule(JavaTimeModule())
        }
    }

    /* Open API docs plugin installation */
    install(OpenAPIGen) {
        info {
            version = "1.0.0"
            title = "Ktor Proxy Server"
        }

        server("http://localhost:6001") {
            description = "Ktor Proxy Server"
        }

        replaceModule(DefaultSchemaNamer, object : SchemaNamer {
            val regex = Regex("[A-Za-z0-9_.]+")

            override fun get(type: KType): String {
                return type
                    .toString()
                    .replace(regex) { it.value.split(".").last() }
                    .replace(Regex(">|<|, "), "_")
            }
        })
    }

    registerRoutes()
}
