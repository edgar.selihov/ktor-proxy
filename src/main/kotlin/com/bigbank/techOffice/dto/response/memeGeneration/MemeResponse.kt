package com.bigbank.techOffice.dto.response.memeGeneration

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class MemeResponse {
    var url: String? = null
}
