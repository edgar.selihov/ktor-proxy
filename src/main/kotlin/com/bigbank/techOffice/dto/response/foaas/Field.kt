package com.bigbank.techOffice.dto.response.foaas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class Field {
    @JsonProperty("name")
    var name: String? = null

    @JsonProperty("field")
    var field: String? = null
}
