package com.bigbank.techOffice.dto.request.meme

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class MemeRequest {
    var topText: String? = null
    var bottomText: String? = null
    var imgUrl: String? = null
}
