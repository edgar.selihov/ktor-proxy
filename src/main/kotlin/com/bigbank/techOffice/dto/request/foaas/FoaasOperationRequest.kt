package com.bigbank.techOffice.dto.request.foaas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class FoaasOperationRequest {
    var url: String? = null

    @JsonProperty("fields")
    var fields: List<FoaasFieldRequest> = listOf()
}
