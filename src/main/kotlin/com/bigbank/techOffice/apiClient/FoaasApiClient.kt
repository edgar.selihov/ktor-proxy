package com.bigbank.techOffice.apiClient

import com.bigbank.techOffice.dto.request.HttpRequest
import com.bigbank.techOffice.dto.response.foaas.Operation
import com.bigbank.techOffice.dto.response.foaas.FoaasQuoteResponse
import com.bigbank.techOffice.exception.ApiClientException
import com.bigbank.techOffice.exception.HttpClientException
import com.bigbank.techOffice.http.HttpClient
import com.bigbank.techOffice.util.JsonUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object FoaasApiClient {

    private val logger: Logger = LoggerFactory.getLogger(FoaasApiClient::class.java)
    private val host: String = "https://foaas.com"

    fun getOperations(): List<Operation> {
        val request = HttpRequest.create(
            mutableMapOf("accept" to "application/json"),
            "$host/operations"
        )

        val response = try {
            HttpClient.get(request)
        } catch (e: HttpClientException) {
            throw ApiClientException("Request failed, message: ${e.message}")
        }

        return if (response.isSuccessful) {
            JsonUtil.deserializeResponse<List<Operation>>(response.body()?.string()) ?: listOf()
        } else {
            logger.error("Request failed, response status: ${response.code()}")
            listOf()
        }
    }

    fun getPoliteQuote(urlPath: String?): FoaasQuoteResponse? {
        if (urlPath.isNullOrEmpty()) {
            logger.error("URL is empty!")
            return null
        }

        val request = HttpRequest.create(
            mutableMapOf("accept" to "application/json"),
            "$host$urlPath"
        )

        val response = try {
            HttpClient.get(request)
        } catch (e: HttpClientException) {
            throw ApiClientException("Request failed, message: ${e.message}")
        }

        return if (response.isSuccessful) {
            JsonUtil.deserializeResponse<FoaasQuoteResponse>(response.body()?.string())
        } else {
            logger.error("Request failed, response status: ${response.code()}")
            null
        }
    }
}
