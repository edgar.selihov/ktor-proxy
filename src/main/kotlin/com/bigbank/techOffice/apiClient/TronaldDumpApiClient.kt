package com.bigbank.techOffice.apiClient

import com.bigbank.techOffice.dto.request.HttpRequest
import com.bigbank.techOffice.dto.response.tronaldDump.TronaldDumpResponse
import com.bigbank.techOffice.exception.ApiClientException
import com.bigbank.techOffice.exception.HttpClientException
import com.bigbank.techOffice.http.HttpClient
import com.bigbank.techOffice.util.JsonUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object TronaldDumpApiClient {

    private val logger: Logger = LoggerFactory.getLogger(TronaldDumpApiClient::class.java)
    private val host: String = "https://tronalddump.io"

    fun getRandomQuote(): TronaldDumpResponse? {
        val request = HttpRequest.create(
            mutableMapOf("accept" to "application/json"),
            "$host/random/quote"
        )

        val response = try {
            HttpClient.get(request)
        } catch (e: HttpClientException) {
            throw ApiClientException("Request failed, message: ${e.message}")
        }

        return if (response.isSuccessful) {
            JsonUtil.deserializeResponse<TronaldDumpResponse>(response.body()?.string())
        } else {
            logger.error("Request failed, response status: ${response.code()}")
            null
        }
    }
}
