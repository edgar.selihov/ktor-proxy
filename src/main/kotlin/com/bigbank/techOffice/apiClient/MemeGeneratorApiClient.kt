package com.bigbank.techOffice.apiClient

import com.bigbank.techOffice.dto.request.HttpRequest
import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.dto.response.memeGeneration.MemeResponse
import com.bigbank.techOffice.exception.ApiClientException
import com.bigbank.techOffice.exception.HttpClientException
import com.bigbank.techOffice.http.HttpClient
import com.bigbank.techOffice.util.JsonUtil
import com.bigbank.techOffice.util.StringUtil
import io.ktor.http.ContentType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.RuntimeException

object MemeGeneratorApiClient {

    private val logger: Logger = LoggerFactory.getLogger(MemeGeneratorApiClient::class.java)
    private val host: String = "https://memebuild.com/api/1.0"
    private val API_KEY: String = System.getenv("ENV_MEME_GENERATOR_API_KEY")
        ?: throw RuntimeException("Meme Generator API key is not set up")

    fun createMeme(memeRequest: MemeRequest): MemeResponse? {
        val bodyAsFormData = StringUtil.convertRequestToFormDataString(memeRequest)
        val request = HttpRequest.create(
            mutableMapOf(
                "API-KEY" to API_KEY,
                "Accept" to "*/*",
                "Content-Type" to "multipart/form-data"
            ),
            "$host/generateMeme",
            ContentType.MultiPart.FormData,
            bodyAsFormData
        )

        val response = try {
            HttpClient.post(request)
        } catch (e: HttpClientException) {
            throw ApiClientException("Request failed, message: ${e.message}")
        }

        return if (response.isSuccessful) {
            JsonUtil.deserializeResponse<MemeResponse>(response.body()?.string())
        } else {
            logger.error("Request failed, response status: ${response.code()}")
            null
        }
    }
}
