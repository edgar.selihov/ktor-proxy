package com.bigbank.techOffice.controller.apiController

import com.bigbank.techOffice.dto.response.StatusResponse
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Routing.apiStatusRouting() {
    get("/status") {
        call.respond(
            StatusResponse().also {
                it.message = "I am alive!"
                it.code = 200
            }
        )
    }
}
