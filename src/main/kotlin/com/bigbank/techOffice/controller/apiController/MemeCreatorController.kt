package com.bigbank.techOffice.controller.apiController

import com.bigbank.techOffice.apiClient.MemeGeneratorApiClient
import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.dto.response.memeGeneration.MemeResponse
import com.bigbank.techOffice.exception.ApiClientException
import com.bigbank.techOffice.exception.RequestBodyValidationException
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respondText
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.post

fun Routing.memeCreatorRouting() {
    post("/meme") {
        val request = call.receive<MemeRequest>()

        try {
            validateRequestBody(request)
        } catch (e: RequestBodyValidationException) {
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode(400, e.message!!)
            )
            return@post
        }

        val response = try {
            MemeGeneratorApiClient.createMeme(request)
        } catch (e: ApiClientException) {
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode(400, e.message!!)
            )
            return@post
        } ?: MemeResponse()

        call.respond(response)
    }
}


private fun validateRequestBody(request: MemeRequest) {
    when {
        request.topText.isNullOrEmpty() && request.bottomText.isNullOrEmpty() ->
            throw RequestBodyValidationException("Top text and bottom text cannot be empty!")
        request.imgUrl.isNullOrEmpty() -> throw RequestBodyValidationException("Meme's template image URL cannot be empty!")
    }
}
