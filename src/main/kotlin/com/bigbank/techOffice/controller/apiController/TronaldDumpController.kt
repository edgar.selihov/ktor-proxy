package com.bigbank.techOffice.controller.apiController

import com.bigbank.techOffice.apiClient.TronaldDumpApiClient
import com.bigbank.techOffice.dto.response.tronaldDump.TronaldDumpResponse
import com.bigbank.techOffice.exception.ApiClientException
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Routing.tronaldDumpRouting() {
    get("/tronald-dump-quote") {

        val response = try {
            TronaldDumpApiClient.getRandomQuote()
        } catch (e: ApiClientException) {
            call.respondText(
                e.localizedMessage,
                ContentType.Application.Json,
                HttpStatusCode(400, e.message!!)
            )
            return@get
        } ?: TronaldDumpResponse()

        call.respond(response)
    }
}
