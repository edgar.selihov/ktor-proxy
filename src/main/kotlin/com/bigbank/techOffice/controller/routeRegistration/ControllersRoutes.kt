package com.bigbank.techOffice.controller.routeRegistration

import com.bigbank.techOffice.controller.apiController.apiStatusRouting
import com.bigbank.techOffice.controller.docs.statusRouting
import com.bigbank.techOffice.controller.docs.tronaldDumpRouting
import com.bigbank.techOffice.controller.docs.foaasRouting
import com.bigbank.techOffice.controller.docs.memeCreatorRouting
import com.bigbank.techOffice.controller.docs.initOpenApiRouting
import com.bigbank.techOffice.controller.apiController.foaasRouting
import com.bigbank.techOffice.controller.apiController.memeCreatorRouting
import com.bigbank.techOffice.controller.apiController.tronaldDumpRouting
import com.papsign.ktor.openapigen.route.apiRouting
import io.ktor.application.Application
import io.ktor.routing.routing

fun Application.registerRoutes() {
    routing {
        apiStatusRouting()
        tronaldDumpRouting()
        foaasRouting()
        memeCreatorRouting()
        initOpenApiRouting()
    }

    /* API routing for open API docs. Do not modify! */
    apiRouting {
        statusRouting()
        tronaldDumpRouting()
        foaasRouting()
        memeCreatorRouting()
    }
}
