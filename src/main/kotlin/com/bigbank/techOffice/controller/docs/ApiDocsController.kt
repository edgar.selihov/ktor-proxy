package com.bigbank.techOffice.controller.docs

import com.bigbank.techOffice.dto.request.foaas.FoaasFieldRequest
import com.bigbank.techOffice.dto.request.foaas.FoaasOperationRequest
import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.dto.response.StatusResponse
import com.bigbank.techOffice.dto.response.foaas.Field
import com.bigbank.techOffice.dto.response.foaas.Operation
import com.bigbank.techOffice.dto.response.foaas.FoaasQuoteResponse
import com.bigbank.techOffice.dto.response.memeGeneration.MemeResponse
import com.bigbank.techOffice.dto.response.tronaldDump.TronaldDumpResponse
import com.papsign.ktor.openapigen.APITag
import com.papsign.ktor.openapigen.route.status
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.tag
import com.papsign.ktor.openapigen.route.info
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import com.papsign.ktor.openapigen.route.path.normal.get
import com.papsign.ktor.openapigen.route.path.normal.post
import com.papsign.ktor.openapigen.route.response.respond
import io.ktor.http.HttpStatusCode
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun NormalOpenAPIRoute.statusRouting() {
    val exampleResponse = StatusResponse().also {
        it.message = "OK"
        it.code = 200
    }

    tag(Tag.Status) {
        route("/ status") {
            get<Unit, StatusResponse>(
                info(
                    summary = "Server status endpoint",
                    description = "Endpoint for checking Ktor server status"
                ),
                status(HttpStatusCode.OK),
                example = exampleResponse
            ) {
                respond(exampleResponse)
            }
        }
    }
}

fun NormalOpenAPIRoute.tronaldDumpRouting() {
    val exampleResponse = TronaldDumpResponse().also {
        it.quoteId = "ergMsoOmSx2c5qx7eUOMgQ"
        it.appearedAt =
            LocalDateTime.parse("2016-05-07T02:44:44.000Z", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
        it.createdAt =
            LocalDateTime.parse("2019-12-13T17:02:12.488Z", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
        it.tags = listOf()
        it.quote =
            "Goofy Elizabeth Warren and her phony Native American heritage are on a Twitter rant. She is too easy! I'm driving her nuts."
    }

    tag(Tag.TronaldDump) {
        route("/ tronald-dump-quote") {
            get<Unit, TronaldDumpResponse>(
                info(
                    summary = "A random Donald Trump's quote",
                    description = "Endpoint to get a random Donald Trump's quote"
                ),
                status(HttpStatusCode.OK),
                example = exampleResponse
            ) {
                respond(exampleResponse)
            }
        }
    }
}

fun NormalOpenAPIRoute.foaasRouting() {
    val exampleOperationResponse = listOf(
        Operation().also {
            it.name = "Asshole"
            it.url = "/asshole/:from"
            it.fields = listOf(
                Field().apply {
                    name = "From"
                    field = ":from"
                }
            )
        }
    )

    val exampleFoaasQuoteRequest = FoaasOperationRequest().also {
        it.url = "/asshole/:from"
        it.fields = listOf(
            FoaasFieldRequest().apply {
                value = "Legolas"
                field = "from"
            }
        )
    }

    val exampleFoaasQuoteResponse = FoaasQuoteResponse().also {
        it.message = "Fuck you, asshole."
        it.subtitle = "- Legolas"
    }

    tag(Tag.FoaasOperations) {
        route("/ foaas-operations") {
            get<Unit, List<Operation>>(
                info(
                    summary = "FOAAS operations",
                    description = "Available FOAAS operations for polite quotes"
                ),
                status(HttpStatusCode.OK),
                example = exampleOperationResponse
            ) {
                respond(exampleOperationResponse)
            }
        }
    }

    tag(Tag.FoaasPoliteQuote) {
        route("/ foaas-polite-quote") {
            post<Unit, FoaasQuoteResponse, FoaasOperationRequest>(
                info(
                    summary = "Get FOAAS polite quote",
                    description = "Get FOAAS polite quote based on user's provided data"
                ),
                status(HttpStatusCode.OK),
                exampleResponse = exampleFoaasQuoteResponse,
                exampleRequest = exampleFoaasQuoteRequest
            ) { _, _ ->
                respond(exampleFoaasQuoteResponse)
            }
        }
    }
}

fun NormalOpenAPIRoute.memeCreatorRouting() {
    val exampleMemeRequest = MemeRequest().also {
        it.topText = "foo"
        it.bottomText = "bar"
        it.imgUrl = "https://static.wikia.nocookie.net/0262aa61-9b8f-4398-bf96-cbe37d0a6ed2"
    }

    val exampleMemeResponse = MemeResponse().also {
        it.url = "someUrlWhereYourMemeIsLocated"
    }

    route("/ meme") {
        tag(Tag.Meme) {
            post<Unit, MemeResponse, MemeRequest>(
                info(
                    summary = "Create your meme",
                    description = "Create a meme with your template image and text"
                ),
                status(HttpStatusCode.OK),
                exampleRequest = exampleMemeRequest,
                exampleResponse = exampleMemeResponse
            ) { _, _ ->
                respond(exampleMemeResponse)
            }
        }
    }
}

enum class Tag(override val description: String) : APITag {
    Status(""),
    TronaldDump("Tronald Dump a random quote"),
    FoaasOperations("FOAAS available operations"),
    FoaasPoliteQuote("FOAAS polite quote"),
    Meme("Create a your own meme")
}
