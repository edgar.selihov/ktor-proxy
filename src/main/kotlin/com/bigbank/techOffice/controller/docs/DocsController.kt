package com.bigbank.techOffice.controller.docs

import com.papsign.ktor.openapigen.openAPIGen
import io.ktor.application.call
import io.ktor.application.application
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Routing.initOpenApiRouting() {
    get("/openapi.json") {
        call.respond(application.openAPIGen.api.serialize())
    }

    get("/") {
        call.respondRedirect("/swagger-ui/index.html?url=/openapi.json", true)
    }
}
