package com.bigbank.techOffice.util

import com.fasterxml.jackson.core.type.TypeReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object StringUtil {

    private val logger: Logger = LoggerFactory.getLogger(StringUtil::class.java)

    fun formatPathParamsForFoaasQuote(
        urlPath: String,
        params: Map<String?, String?>
    ): String? {
        var modifiedUrlPath = urlPath

        when {
            urlPath.isEmpty() -> {
                logger.warn("URL path is empty!")
                return null
            }
            params.isNullOrEmpty() -> {
                logger.warn("No URL params were received!")
                return null
            }
        }

        params.forEach { param ->
            if (modifiedUrlPath.contains(param.key!!)) {
                modifiedUrlPath = modifiedUrlPath.replace(param.key!!, param.value!!)
            }
        }

        return modifiedUrlPath.replace(":", "")
    }

    fun convertRequestToFormDataString(obj: Any): String {
        val objectAsMap = JsonUtil.objectMapper.convertValue(obj, object : TypeReference<Map<String, Any>>() {})

        return objectAsMap
            .map { (k, v) -> "$k=$v" }
            .joinToString("&")
    }
}
