package com.bigbank.techOffice.util

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object JsonUtil {
    val objectMapper = ObjectMapper()
    val logger: Logger = LoggerFactory.getLogger(JsonUtil::class.java)

    inline fun <reified T> deserializeResponse(json: String?): T? {
        return try {
            objectMapper.readValue(json, object : TypeReference<T>() {})
        } catch (e: JsonProcessingException) {
            logger.error("JSON parsing failed: ${e.message}", e)
            null
        }
    }
}
