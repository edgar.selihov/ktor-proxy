package com.bigbank.techOffsite.controllerTests

import com.bigbank.techOffice.util.StringUtil
import org.junit.Test
import kotlin.test.assertEquals

class StringUtilTest {

    @Test
    fun testIfUrlPathIsBuiltCorrectly() {
        val params = getMapOfUrlParams()
        val result = StringUtil.formatPathParamsForFoaasQuote("/absolutely/:company/:from", params)

        assertEquals("/absolutely/ACME/John%20Doe", result)
    }

    @Test
    fun testIfFormDataStringWrittenCorrectly() {
        val result = StringUtil.convertRequestToFormDataString(getTestRequestBody())

        assertEquals("one=two&three=four&five=six", result)
    }

    private fun getMapOfUrlParams(): Map<String?, String?> {
        return mapOf(
            ":company" to "ACME",
            ":from" to "John%20Doe"
        )
    }

    private fun getTestRequestBody(): Map<String, String> {
        return mapOf(
            "one" to "two",
            "three" to "four",
            "five" to "six"
        )
    }
}
