package com.bigbank.techOffsite.controllerTests

import com.bigbank.techOffice.apiClient.FoaasApiClient
import com.bigbank.techOffice.apiClient.MemeGeneratorApiClient
import com.bigbank.techOffice.apiClient.TronaldDumpApiClient
import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.util.StringUtil
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals

class ApiClientTest {

    @Test
    fun getTronaldDumpRandomQuoteTest() {
        val randomQuote = TronaldDumpApiClient.getRandomQuote()

        assertNotNull(randomQuote)
    }

    @Test
    fun getFoaasOperationsTest() {
        val operations = FoaasApiClient.getOperations()

        assertFalse(operations.isEmpty())
    }

    @Test
    fun getFoaasPoliteQuoteTest() {
        val params = getMapOfUrlParams()
        val formattedUrlPath = StringUtil.formatPathParamsForFoaasQuote("/absolutely/:company/:from", params)
        val getPoliteQuote = FoaasApiClient.getPoliteQuote(formattedUrlPath)

        assertEquals("Absolutely fucking Not, ACME, No Fucking Way!", getPoliteQuote?.message)
        assertEquals("- John Doe", getPoliteQuote?.subtitle)
    }

    @Test
    @Ignore
    fun createMemeTest() {
        val meme = MemeGeneratorApiClient.createMeme(createMemeRequest())

        assertNotNull(meme)
        assertNotNull(meme!!.url)
    }

    private fun getMapOfUrlParams(): Map<String?, String?> {
        return mapOf(
            ":company" to "ACME",
            ":from" to "John%20Doe"
        )
    }

    private fun createMemeRequest(): MemeRequest {
        return MemeRequest().also {
            it.topText = "foo"
            it.bottomText = "bar"
            it.imgUrl = "https://static.wikia.nocookie.net/0262aa61-9b8f-4398-bf96-cbe37d0a6ed2"
        }
    }
}
