package com.bigbank.techOffsite.controllerTests

import com.bigbank.techOffice.dto.request.foaas.FoaasFieldRequest
import com.bigbank.techOffice.dto.request.foaas.FoaasOperationRequest
import com.bigbank.techOffice.dto.request.meme.MemeRequest
import com.bigbank.techOffice.dto.response.foaas.Operation
import com.bigbank.techOffice.dto.response.foaas.FoaasQuoteResponse
import com.bigbank.techOffice.dto.response.memeGeneration.MemeResponse
import com.bigbank.techOffice.dto.response.tronaldDump.TronaldDumpResponse
import com.bigbank.techOffice.module
import com.bigbank.techOffice.util.JsonUtil
import io.ktor.application.Application
import io.ktor.http.HttpStatusCode
import io.ktor.http.HttpMethod
import io.ktor.http.HttpHeaders
import io.ktor.http.ContentType
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import io.ktor.server.testing.setBody
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

class ControllerTest {

    @Test
    fun getStatusTest() {
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/status").apply {
                assertEquals("""{"message":"I am alive!","code":200}""", response.content)
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    fun getGetTronaldDumpQuoteTest() {
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/tronald-dump-quote").apply {
                val testResponse = JsonUtil.deserializeResponse<TronaldDumpResponse>(response.content)

                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(testResponse)
                assertNotNull(testResponse.quote)
            }
        }
    }

    @Test
    fun getFoaasOpertionsTest() {
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/fooas-operations").apply {
                val testResponse = JsonUtil.deserializeResponse<List<Operation>>(response.content) ?: listOf()

                assertEquals(HttpStatusCode.OK, response.status())
                assertFalse(testResponse.isEmpty())
            }
        }
    }

    @Test
    fun postFoaasQuoteTest() {
        val testRequest = buildRequestBodyForFoaasQuote()
        withTestApplication(Application::module) {
            with(handleRequest(HttpMethod.Post, "/foaas-polite-quote") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(testRequest)
            }) {
                val testResponse = JsonUtil.deserializeResponse<FoaasQuoteResponse>(response.content)

                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(testResponse)
                assertThat(testResponse.subtitle, containsString("Legolas"))
            }
        }
    }

    @Test
    @Ignore
    fun postCreateMemeTest() {
        withTestApplication(Application::module) {
            with(handleRequest(HttpMethod.Post, "/meme") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(buildMemeRequestBody())
            }) {
                val testResponse = JsonUtil.deserializeResponse<MemeResponse>(response.content)

                assertEquals(HttpStatusCode.OK, response.status())
                assertNotNull(testResponse?.url)
            }
        }
    }

    private fun buildRequestBodyForFoaasQuote(): String {
        val requestBody = FoaasOperationRequest().also { request ->
            request.url = "/asshole/:from"
            request.fields = listOf(
                FoaasFieldRequest().also {
                    it.field = "from"
                    it.value = "Legolas"
                }
            )
        }

        return JsonUtil.objectMapper.writeValueAsString(requestBody)
    }

    private fun buildMemeRequestBody(): String {
        val requestBody = MemeRequest().also {
            it.topText = "Very nice"
            it.bottomText = "I like it"
            it.imgUrl = "https://icdn.lenta.ru/images/2017/11/21/18/20171121182650506/detail_55d6a502d3a9cb30f04272423ab95cdd.jpg"
        }

        return JsonUtil.objectMapper.writeValueAsString(requestBody)
    }
}
